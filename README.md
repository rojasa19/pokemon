## Pokemon finder - Frontend

- Todo el proyecto React se encuentra en el carpeta **src**
- El build del mismo se cuentra en la carpeta **build**.
- Para el front se implemento Redux para manejar los datos.
- Los test se centraron en controlar el store.

**Plus:**

- Por ultimo se agrego un plus de integración continua con gitlab.
- La misma ejecuta el build, corre los test y sube todo a AWS S3, a un bucket especifico.
- Además se creo un cloudfront para cachear el frontend.
- Todo está en el .gitlab-ci.yml
import { ADD_POKEMON } from "../constants/action-types";
const initialState = {
  pokemons: []
};

const reducerPokemon = (state = initialState, action) => {
  if (action.type === ADD_POKEMON) {
    return { ...state, pokemons: action.payload};
  }else {
    return state;
  }
};

export default reducerPokemon;
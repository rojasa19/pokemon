import React from 'react';
import {Provider} from 'react-redux';
import store from './store/index';
import Form from "./components/form"
import Pokemons from "./components/pokemons";
import Powerby from "./components/Powerby/powerby";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container">
          <h1 className="mt-4">Pokemon finder</h1>
          <p>El que quiere pokemons, que los busque</p>
          <Form/>
          <Pokemons/>
          <hr/>
          <Powerby/>
        </div>
      </Provider>
    );
  }
}
export default App;
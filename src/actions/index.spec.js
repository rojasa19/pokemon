import reducerPokemon from '../reducers/index';
import {addPokemon} from './index';
import {ADD_POKEMON} from '../constants/action-types';

describe('Pokemon actions', () => {
    describe('addPokemons from action', () => {
        it('ADD_POKEMON, agrega los pokemons e inicializa el state', () => {
            const pokemons = [
                {
                    "name": "mewtwo",
                    "url": "https://pokeapi.co/api/v2/pokemon/150/"
                },
                {
                    "name": "mew",
                    "url": "https://pokeapi.co/api/v2/pokemon/151/"
                },
                {
                    "name": "mewtwo-mega-x",
                    "url": "https://pokeapi.co/api/v2/pokemon/10043/"
                },
                {
                    "name": "mewtwo-mega-y",
                    "url": "https://pokeapi.co/api/v2/pokemon/10044/"
                }
            ];
            expect(addPokemon(pokemons, ADD_POKEMON)).toEqual({
                type: 'ADD_POKEMON',
                payload: pokemons
            });
        });
    });
});

describe('Pokemon reduce', () => {
    describe('addPokemons from reduce', () => {
        it('ADD_POKEMON, agrega los pokemons e inicializa el state', () => {
            const pokemons = [
                {
                    "name": "mewtwo",
                    "url": "https://pokeapi.co/api/v2/pokemon/150/"
                },
                {
                    "name": "mew",
                    "url": "https://pokeapi.co/api/v2/pokemon/151/"
                },
                {
                    "name": "mewtwo-mega-x",
                    "url": "https://pokeapi.co/api/v2/pokemon/10043/"
                },
                {
                    "name": "mewtwo-mega-y",
                    "url": "https://pokeapi.co/api/v2/pokemon/10044/"
                }
            ];
            expect(reducerPokemon(undefined, addPokemon(pokemons, ADD_POKEMON))).toEqual({
                pokemons: pokemons
            });
        });
    });
});
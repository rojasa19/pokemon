import { ADD_POKEMON } from "../constants/action-types";

export const addPokemon = (payload) => {
  return { type: ADD_POKEMON, payload }
};
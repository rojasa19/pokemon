import { createStore } from "redux";
import reducerPokemon from "../reducers/index";

const store = createStore(reducerPokemon);

export default store;
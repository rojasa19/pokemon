import React from 'react';
import './pokemon.css';

class Pokemon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            image: 'https://i.pinimg.com/564x/a9/4f/4d/a94f4d75a2e429a20838d28d2ae2b996.jpg'
        }
    }
    async componentWillMount() {
        await this.handleSearchByName();
    }

    handleSearchByName = async () => {
        try {
            let res = await fetch(`http://ec2-18-237-219-2.us-west-2.compute.amazonaws.com/pokemon/${this.state.name}/data`);
            let data = await res.json();
            if(data.sprites.front_default) {
                this.setState({ image: data.sprites.front_default });
            }
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <div className="poke-card">
                <img 
                    src={this.state.image}
                    alt={this.state.name}
                />
                <span>{this.state.name}</span>
            </div>
        )
    }
}
export default Pokemon;
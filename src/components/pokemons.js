import React from 'react';
import { connect } from 'react-redux';
import Pokemon from './pokemon/pokemon';

class Pokemons extends React.Component {
    render() {
        return(
            <section>
                <h3 className="my-5">Resultados de busqueda</h3>
                { this.props.pokemons.map((pokemon, i) => ( 
                        <Pokemon key={i} name={pokemon.name}/> 
                    )
                )}
            </section>
        )
    }
}

const mapStateToProps = state => ({ pokemons: state.pokemons })

export default connect(mapStateToProps) (Pokemons);
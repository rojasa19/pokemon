import React from 'react';
import { connect } from 'react-redux';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            img: '#'
        }
    }

    handleName = (event) => {
        this.setState({
            name: event.target.value
        });
        this.props.addPokemon([]);
    }

    handleBuscar = async () => {
        try {
            let res = await fetch(`http://ec2-18-237-219-2.us-west-2.compute.amazonaws.com/pokemon/${this.state.name}/list`);
            let data = await res.json();
            this.props.addPokemon(data);
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <div>
                <div className="input-group my-3">
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="¿Que pokemon estás buscando?"
                        value={this.state.name}
                        onChange={this.handleName}
                    />
                    <div className="input-group-append">
                        <button 
                            className="btn btn-danger" 
                            type="button"
                            onClick={this.handleBuscar}
                        >Buscar</button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPokemon: (value) => dispatch({
            type: 'ADD_POKEMON',
            payload: value 
        })
    }
}
export default connect(null, mapDispatchToProps) (Form);;
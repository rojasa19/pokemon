import React from 'react';
import './powerby.css'

class Powerby extends React.Component {

    handleGoToRepoFront = () => {
        window.open('https://gitlab.com/rojasa19/pokemon', '_blank');
    }

    handleGoToRepoApi = () => {
        window.open('https://gitlab.com/rojasa19/pokemon-api', '_blank');
    }

    render() {
        return (
            <div className="footer">
                <span>Power by Adrián Rojas</span>
                <button 
                    className="btn btn-danger" 
                    type="button"
                    onClick={this.handleGoToRepoFront}
                >Link al repo Front</button>
                <button 
                    className="btn btn-danger" 
                    type="button"
                    onClick={this.handleGoToRepoApi}
                >Link al repo Api</button>
            </div>
        )
    }
}

export default Powerby;